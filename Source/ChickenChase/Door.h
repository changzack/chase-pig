// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Door.generated.h"

UCLASS()
class CHICKENCHASE_API ADoor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ADoor();
	UPROPERTY(VisibleAnywhere, Category = "Trigger Press", BlueprintReadWrite, meta = (AllowPrivateAccess = "True"))
	bool IsDoorOpen;
	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* Mesh;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void TriggerEnter();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void TriggerExit();


private:
	UPROPERTY(VisibleAnywhere, Category = "Trigger Press", BlueprintReadWrite, meta = (AllowPrivateAccess = "True"))
	int PressedCount;
	UPROPERTY(EditAnywhere, Category = "Trigger Press", BlueprintReadOnly, meta = (AllowPrivateAccess = "True"))
	int PressedThreshold;
};
