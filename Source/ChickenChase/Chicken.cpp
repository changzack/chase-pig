// Fill out your copyright notice in the Description page of Project Settings.


#include "Chicken.h"

// Sets default values for this component's properties
UChicken::UChicken()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UChicken::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UChicken::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

